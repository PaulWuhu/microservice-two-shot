import React, { useEffect, useState } from 'react'

const DetailHat = ({hatID, deleteHat, setDeleteHat}) => {
    const id = hatID
    const url =`http://localhost:8090/api/hat/${id}`
    const [detailHat,setDetailHat] = useState({
      fabric:"",
      picture_url:"",
      style_name:"",
      color:"",
      locations:""
    })

    const fetchData= async () =>{ 
        try{
            const response = await fetch(url)
            if(response.ok){
                const data=await response.json()
                setDeleteHat(false)
                setDetailHat({
                  fabric: data.fabric,
                  picture_url: data.picture_url,
                  style_name: data.style_name,
                  color: data.color,
                  locations: data.location.closet_name,
                }
                  )
            }
            else{
                console.error(response)
            }
        }
        catch(e){
            console.log(e)
        }
    }

    useEffect(() => {
        fetchData()
    },[hatID]);

    
    const deleteData = async () =>{
        const response = await fetch(url, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          }
        });
        if(response.ok){
            setDeleteHat(true)
        }
    }
    
    const handleDelete = async (e)=>{
        e.preventDefault();
        deleteData(url)
    }
  return (
    <>
  {!deleteHat &&
    <div className="card mb-3 shadow">
      <img alt="here are some pic for our HAT" src={detailHat.picture_url} style={{"maxWidth":"300px","maxHeight":"300px"}}className='card-img-top'/>
        <div className="card-body">
          <h1 className="card-title"> This hat is from {detailHat.locations}</h1>
          <h2 className="card-subtitle mb-2 text-muted">
          style name:  {detailHat.style_name}
          </h2>
          <h2>
          fabric: {detailHat.fabric}
          </h2>
          <h2 className="card-subtitle mb-2 text-muted">
              Color: {detailHat.color}
          </h2>
          <form  onSubmit={handleDelete} >
            <button type="submit" className="btn-danger">DELETE THIS HAT</button>
          </form>
        </div>
    </div>
    }
    </>
  )
}

export default DetailHat
