import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import CreateHat from './CreateHat';

function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}/>
          <Route path="hats" element={<HatList/>}/>
          {/* another way could make it work with use param, need to checkout how to do that  */}
          <Route exact path="hats/create/" element={<CreateHat/>}/>   
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
