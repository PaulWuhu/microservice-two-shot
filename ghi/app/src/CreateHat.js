import React, { useEffect, useState } from 'react'

const CreateHat = () => {
  const [fabric,setFabric] = useState('')
  const [locations,setLocations] = useState([])
  const [pictureUrl,setPictureUrl] = useState('')
  const [styleName,setStyleName] = useState('')
  const [color,setColor] = useState('')
  const [selectLocation,setSlectLocation] = useState('')
    const fetchData= async () =>{ 
        try{
            const response = await fetch('http://localhost:8100/api/locations/')
            if(response.ok){
                const data=await response.json()
                setLocations(data.locations)
            }
            else{
                console.error(response)
            }
        }
        catch(e){
            console.log(e)
        }
    }
    
    useEffect(() => {
        fetchData();
      },[]);   
    
    const handleFabricChange = (e) => {
        const value = e.target.value;
        setFabric(value);
    }
    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    }
    const handleStyleNameChange = (e) => {
        const value = e.target.value;
        setStyleName(value);
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }
    const handleSelectLocation = (e) => {
        const value = e.target.value;
        setSlectLocation(value);
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric=fabric
        data.picture_url=pictureUrl
        data.style_name=styleName
        data.color=color
        data.location=selectLocation
        const hatURL = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method:'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        }
        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
            setFabric('')
            setColor('')
            setPictureUrl('')
            setStyleName('')
            setSlectLocation('')
            
        }
        else{
            console.error("response not ok")
        }
      }
  return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New HAT</h1>
          <form onSubmit={handleSubmit}id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} value={fabric} placeholder="fabric" name="fabric" required type="text" id="fabric" className="form-control"/>
              <label htmlFor="fabric">fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} value={pictureUrl}placeholder="The picture of your hat" name="starts" required type="url" id="picture_url" className="form-control"/>
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="mm/dd/yyyy" value={color} name="color" required id="color" className="form-control"/>
              <label htmlFor="color">color</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <input onChange={handleStyleNameChange} placeholder="The name of our style" name="styleName" value={styleName} required type="textarea" id="styleName" className="form-control"/>
            </div>
            <div className="mb-3">
              <select onChange={handleSelectLocation} value={selectLocation} required id="location" name="location" className="form-select">
                <option value="">Choose a Closet</option>
                {locations.map(location => {
                      return (
                      <option key={location.href} value={location.href}>
                          {location.closet_name}
                      </option>
                      );
                  })}
              </select>
            </div>
            <button className="btn btn-primary" type="submit">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  )
}

export default CreateHat
