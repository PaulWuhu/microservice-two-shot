import React, { useEffect, useState } from 'react'
import DetailHat from './DetailHat';
import { Link } from 'react-router-dom'

const HatList = () => {
    const [hatHere,setHatHere]= useState(false)
    const [render,setRender]=useState(false)
    const [listData, setListData]= useState([])
    const [deleteHat, setDeleteHat] = useState(false)
    const [selectLocation,setSelectLocation] = useState('')
    const [locations,setLocations] = useState([])
    const [hatID, setHatId] = useState(0)

    const fetchLocationData= async () =>{ 
        try{
            const response = await fetch('http://localhost:8100/api/locations/')
            if(response.ok){
                const data=await response.json()
                setLocations(data.locations)
                
            }
            else{
                console.error(response)
            }
        }
        catch(e){
            console.log(e)
        }
    }
 
    const handleSelectLocation = (e) => {
        const value = e.target.value;
        setSelectLocation(value);
      }
    
      useEffect(() => {
          fetchLocationData();
        },[]);

    const fetchLocationHatsData= async () =>{ 
        try{
            if(selectLocation !==""){
            const response = await fetch(`http://localhost:8090/${selectLocation}hats/`)
        
            if(response.ok){
                const ListData=await response.json()
                setListData(ListData)
                setRender(true)
                if(ListData.hats.length>0){
                    setHatHere(true)
                }
                else{
                    setHatHere(false)
                }
            }
            else{
                console.log("response 404 expected not a bug")
            }
            }
        }
        catch(e){
            console.log(e)
        }
    }

    useEffect(() => {
        fetchLocationHatsData()
    }, [deleteHat]);

    useEffect(() => {
        fetchLocationHatsData()
    }, [selectLocation]);

    const handleClick = (e)=>{
        const value = e.target.alt
        setHatId(value)
    }

  return (
    <div>
    <form onSubmit={ (e) => {
        e.preventDefault()
    }}>
        <div className="mb-3">
        <select onChange={handleSelectLocation} value={selectLocation} required id="location" name="location" className="form-select">
        <option value="">Choose a Closet</option>
        {locations.map(location => {
                return (
                <option key={location.href} value={location.href}>
                    {location.closet_name}
                </option>
                );
            })}
        </select>
        </div>
    </form>
    {selectLocation === '' ? <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-5"> Choose a Closet to see the Hats</div>: 
    <div>
      
           { !hatHere &&
           <div className="px-4 py-5 my-5 mt-0 text-center bg-info display-5"> 
                There are no Hats here in this closet, go Create a New Hat
                <Link className="nav-link" to="create/"> Create a New Hat</Link>
            </div> 
            }
            <div>
            {
            render && 
            <table>
                <tbody>
                <tr>
                    {listData.hats.map((hat) => (
                    <td key={hat.id}>    
                        <img onClick={handleClick} alt={hat.id} src={hat.picture_url} style={{"maxWidth":"300px","maxHeight":"300px"}}/>
                    To check out this hat, click on the picture 
                    </td>
                    ))}
                    </tr>
                </tbody>
            </table>
            }
            </div>
            </div>
        }
        {hatID !== 0 && selectLocation !== '' &&
        <DetailHat hatID={hatID} deleteHat={deleteHat} setDeleteHat={setDeleteHat}/>}
    </div>
    )
    }

export default HatList











//  we need a drop down at the top to select which closet to choose from 
// then display all the hat from that closet 
// we can use if statement with the id= id filter 
// display the picture of the hat 
// picture will be a link to the hat detail
