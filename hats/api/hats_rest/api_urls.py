from django.urls import path
from .views import api_list_hat,api_show_hat

urlpatterns = [
    path("hats/", api_list_hat, name="create_api_hat"),
    path("locations/<int:location_vo_id>/hats/",api_list_hat, name="list_api_hat"),
    path("hat/<int:pk>/", api_show_hat, name="api_show_hat"),
]
