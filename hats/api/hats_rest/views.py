from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import LocationVO, Hat
from common.json import ModelEncoder

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "picture_url",
        "style_name",
        "color",
        "location",
        "id"
    ]
    encoders = {
        "location":LocationVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hat(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hat = Hat.objects.filter(location=location_vo_id)
        else:
            hat = Hat.objects.all()
        return JsonResponse(
            {"hats": hat},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            print(content)
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET"])
def api_show_hat(request, pk):
    if request.method == "GET":
        location = Hat.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=HatEncoder,
            safe=False,
        )
    else: 
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
