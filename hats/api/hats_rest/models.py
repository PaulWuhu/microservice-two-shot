from django.db import models
from django.urls import reverse
# Create your views here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
    )
    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})
